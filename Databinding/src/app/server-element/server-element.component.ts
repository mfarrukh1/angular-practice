import { 
  Component, 
  OnInit, 
  Input, 
  ViewEncapsulation, 
  OnChanges, 
  SimpleChanges, 
  DoCheck, 
  AfterContentInit, 
  AfterContentChecked, 
  AfterViewInit, 
  AfterViewChecked, 
  ContentChild,
  ElementRef} 
  from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ServerElementComponent implements
OnInit, 
OnChanges,
DoCheck, 
AfterContentInit, 
AfterContentChecked, 
AfterViewInit, 
AfterViewChecked {

  @Input('srvElement')
  element!: { type: string; name: string; content: string; };
  @Input('name')
  name!: string;

  @ContentChild('contentParagraph')
  contentParagraph!: ElementRef;
  constructor() { 
    console.log("constructor called");
  }
  ngAfterViewChecked(): void {
    console.log("ngAfterViewChecked called");
  }
  ngAfterViewInit(): void {
    console.log("ngAfterViewInit called");
  }
  ngAfterContentChecked(): void {
    console.log("ngAfterContentChecked called");
  }
  ngAfterContentInit(){
    console.log("ngAfterContentInit called");
    console.log("text content of a paragraph", this.contentParagraph.nativeElement.value);
  }
 
  ngOnChanges(changes: SimpleChanges){
    console.log("ngOnchanges called");
    console.log("ngOnchanges called changes", changes);
  }

  ngOnInit(): void {
    console.log("ngOnInit called");
    console.log("text content of a paragraph", this.contentParagraph.nativeElement.value);
  }
  ngDoCheck(){
    console.log("ngDoCheck called");
  }

  

}
