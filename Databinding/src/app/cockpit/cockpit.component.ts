import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css'],
})
export class CockpitComponent implements OnInit {
  @Output() serverCreated = new EventEmitter<{
    serverName: string;
    serverContent: string;
  }>();
  @Output() blueprintCreated = new EventEmitter<{
    serverName: string;
    serverContent: string;
  }>();
  newServerName = '';
  newServerContent = '';

  @ViewChild('serverContentInput')
  serverContentInput!: ElementRef;
  constructor() {}

  ngOnInit(): void {}

  

  onAddServer(serverVal: HTMLInputElement) {
    this.serverCreated.emit({
      // serverName: this.newServerName,
      // OR
      serverName: serverVal.value,
      // serverContent: this.newServerContent,
      serverContent: this.serverContentInput.nativeElement.value,
    });
  }
  onAddBlueprint(serverVal: HTMLInputElement) {
    this.blueprintCreated.emit({
      // serverName: this.newServerName,
      // OR
      serverName: serverVal.value,
      // serverContent: this.newServerContent,
      serverContent: this.serverContentInput.nativeElement.value,
    });
  }
}
