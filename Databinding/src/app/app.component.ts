import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Databinding';
  serverElements:any =[{type: 'server', name:'test server', content: 'just a test!'}];
  oddNum:any[] =[];
  evenNum:any[] =[];

  onServerAdded(serverData: {serverName:string, serverContent: string}){
    this.serverElements.push({
      type: 'server',
      name: serverData.serverName,
      content: serverData.serverContent
    });
  }
  onBlueprintAdded(blueprintData: {serverName:string, serverContent: string}){
    this.serverElements.push({
      type: 'blueprint',
      name: blueprintData.serverName,
      content: blueprintData.serverContent
    });
  }
  onChangeFirst(){
    this.serverElements[0].name="changed";
  }
  onDestroyFirst(){
    this.serverElements.splice(0, 1);
  }
  ngOnInit(): void {
    this.oncreateServer;
  }
  oncreateServer(value: any){
    console.log("parent called", value);
    if(value % 2 == 0){
      this.evenNum.push(value);
    }
    else{
      this.oddNum.push(value);
    }
    console.log("odd called", this.oddNum);
    console.log("even called", this.evenNum);
  }
}
