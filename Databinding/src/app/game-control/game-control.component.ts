import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {

  inc: number = 0;
  stat: any;

  @Output() newItemEvent = new EventEmitter<number>();
  constructor() { }

  ngOnInit(): void {
    // this.newItemEvent.emit(this.inc);
  }
  
  start(){
     this.stat = setInterval(()=>{
      this.inc = this.inc + 1;
      this.newItemEvent.emit(this.inc)
      console.log(this.inc);
    },1000)
  }
  stop(){
    clearInterval(this.stat);
    this.inc = 0;
  }

}
