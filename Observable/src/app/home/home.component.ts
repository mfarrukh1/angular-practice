import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  private firstObsSubscription!: Subscription;
  constructor() {}

  ngOnInit() {
    // Builtin Observable
    // this.firstObsSubscription = interval(1000).subscribe((count)=>{
    //   console.log(count);
    // })

    // Custom Observable
    const customIntervalObservable = Observable.create((observer: any) => {
      let count = 0;
      setInterval(() => {
        observer.next(count);
        if (count === 5) {
          observer.complete();
        }
        if (count > 3) {
          observer.error(new Error('Count is greater than 3!'));
        }
        count++;
      }, 1000);
    });
    // customIntervalObservable.pipe(map((data: number)=>{
    //   return 'Round ' + (data + 1);
    // }));
    this.firstObsSubscription = customIntervalObservable
      .pipe(
        filter((data: any) => {
          return data > 0;
        }),
        map((data: number) => {
          return 'Round ' + (data + 1);
        })
      )
      .subscribe(
        (data: any) => {
          console.log(data);
        },
        (error: any) => {
          alert(error.message);
        },
        () => {
          alert('Completed');
        }
      );
  }

  ngOnDestroy(): void {
    this.firstObsSubscription.unsubscribe();
  }
}
