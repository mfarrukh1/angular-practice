import { TestBed } from "@angular/core/testing";
import { UserComponent } from "../user/user.component";
import { ReversePipe } from "./reverse.pipe";

describe('Component: User', () => {
  it('Should create the app', ()=>{
    let reversePipe = new ReversePipe();
    expect(reversePipe.transform('hello')).toEqual('olleh');
  });

});


