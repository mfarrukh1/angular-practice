import { Pipe } from "@angular/core";

@Pipe({
    name: 'ReversePipe'
})

export class ReversePipe {
    transform(value: string){
        return value.split("").reverse().join("");
    }
}