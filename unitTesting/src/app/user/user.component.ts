import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/data.service';
import { USerService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers:[USerService, DataService]
})
export class UserComponent implements OnInit {

  user!: {name: string};
  isLoggedIn = false;
  data!: string;
  componentName: string = 'User Component';

  constructor(private userService: USerService, private dataService: DataService) { }

  ngOnInit(): void {
    this.user = this.userService.user;  
    this.dataService.getDetail().then((data: string)=>{
      this.data = data;
      console.log("dataService =>", data );
    })
  }

  sum(){
    return 100;
  }
  dynamicalySum(a:number, b: number){
     return a+b;
  }

}
