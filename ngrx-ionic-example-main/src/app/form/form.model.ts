export interface FormDataModel {
    id: string,
    name: string,
    email: string,
}

// export class FormDataModel {
//   id: string,
//   name: string | any;
//   email: string | any;

//   constructor(id: string, name: string, email: string) {
//     this.id = id;
//     this.name = name;
//     this.email = email;
//   }
// }
