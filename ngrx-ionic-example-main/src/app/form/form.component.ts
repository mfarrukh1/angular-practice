import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { addTodo, removeTodoForm, submitForm } from '../state/todos/todo.actions';
import { selectForm } from '../state/todos/todo.selectors';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  signupForm!: FormGroup;
  public alldatas$ = this.store.select(selectForm);
  public todo = '';
  array;
  constructor(private store: Store ) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'email':new FormControl(null, Validators.required),
    });

    
   
  }

  onSubmit(){
    this.store.dispatch(submitForm({ todosForm: this.signupForm.value }));
    this.todo = '';
    console.log("form data =>", this.signupForm.value);
  
  }
  removeTodoForm(id: string) {
    this.store.dispatch(removeTodoForm({ id: id }));
  }

}
