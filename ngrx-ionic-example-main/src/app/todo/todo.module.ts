import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TodoPage } from './todo.page';

import { TodoPageRoutingModule } from './todo-routing.module';
import { FormComponent } from '../form/form.component';

@NgModule({
  imports: [CommonModule,ReactiveFormsModule, FormsModule, IonicModule, TodoPageRoutingModule],
  declarations: [TodoPage, FormComponent],
})
export class TodoPageModule {}
