import { createReducer, on } from '@ngrx/store';
import {
  addTodo,
  removeTodo,
  loadTodos,
  loadTodosSuccess,
  loadTodosFailure,
  submitForm,
  removeTodoForm,
} from './todo.actions';
import { Todo } from '../../todo/todo.model';
import { FormDataModel } from 'src/app/form/form.model';

export interface TodoState {
  todos: Todo[];
  formData: FormDataModel[];
  error: string;
  status: 'pending' | 'loading' | 'error' | 'success';
}

export const initialState: TodoState = {
  todos: [],
  formData: [],
  error: null,
  status: 'pending',
};

export const todoReducer = createReducer(
  // Supply the initial state
  initialState,
  // Add the new todo to the todos array
  on(addTodo, (state, { content }) => ({
    ...state,
    todos: [...state.todos, { id: Date.now().toString(), content: content }],
  })),

  on(submitForm, (state, { todosForm }) => {
   
    debugger;

    return {
      ...state,
      formData: [
        ...state.formData,
        {id: Date.now().toString(), name: todosForm['name'], email: todosForm['email'] },
      ],
    };
  }),

  // Remove the from data from the todosForm array
  on(removeTodoForm, (state, {id})=>{
    debugger
    return{
      ...state,
      formData: state.formData.filter((todo) => todo.id !== id),
    }
  }),
  // on(removeTodoForm, (state, { id }) => ({
  //   ...state,
  //   formData: state.formData.filter((todo) => todo.id !== id),
  // })),

  // Remove the todo from the todos array
  on(removeTodo, (state, { id }) => ({
    ...state,
    todos: state.todos.filter((todo) => todo.id !== id),
  })),
  // Trigger loading the todos
  on(loadTodos, (state) => ({ ...state, status: 'loading' })),
  // Handle successfully loaded todos
  on(loadTodosSuccess, (state, { todos }) => ({
    ...state,
    todos: todos,
    error: null,
    status: 'success',
  })),
  // Handle todos load failure
  on(loadTodosFailure, (state, { error }) => ({
    ...state,
    error: error,
    status: 'error',
  }))
);
