import { Injectable } from "@angular/core";
import { CounterService } from "./app/Counter.service";

@Injectable()



export class UserService {

    constructor(private countSer: CounterService){

    }

    activeUsers = ['Max', 'Anna'];
    inactiveUsers = ['Chris', 'Manu'];

    setToActive(id: number){
        this.activeUsers.push(this.inactiveUsers[id]);
        this.inactiveUsers.splice(id, 1);
        this.countSer.incrementInactiveToActive();
    }
    setToInactive(id: number){
        this.inactiveUsers.push(this.activeUsers[id]);
        this.activeUsers.splice(id, 1);
        this.countSer.incrementActiveToInactive();
    }
}