import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AccountService } from './account.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoggingService } from './logging.service';
import { TestComponent } from './test/test.component';
import { ActiveComponent } from './active/active.component';
import { InactiveComponent } from './inactive/inactive.component';
import { CounterService } from './Counter.service';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    ActiveComponent,
    InactiveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  // providers: [CounterService],
  providers: [AccountService, LoggingService, CounterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
