import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/users.service';

@Component({
  selector: 'app-inactive',
  templateUrl: './inactive.component.html',
  styleUrls: ['./inactive.component.css']
})
export class InactiveComponent implements OnInit {
  users: string[] =[];
  
  constructor(private userSer: UserService) { }

  ngOnInit(){
    this.users = this.userSer.inactiveUsers;
  }

  onSetToActive(id: number){
    this.userSer.setToActive(id);
  }

}
