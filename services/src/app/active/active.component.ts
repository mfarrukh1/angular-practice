import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/users.service';

@Component({
  selector: 'app-active',
  templateUrl: './active.component.html',
  styleUrls: ['./active.component.css'],
})
export class ActiveComponent implements OnInit {
  userss: string[] = [];
  constructor(private userSer: UserService) {}

  ngOnInit(): void {
    this.userss = this.userSer.activeUsers;
  }
  onSetToInActive(id: number){
    this.userSer.setToInactive(id);
  }
}
