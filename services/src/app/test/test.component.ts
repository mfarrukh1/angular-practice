import { Component, Input, OnInit } from '@angular/core';
import { NumberValueAccessor } from '@angular/forms';
import { AccountService } from '../account.service';
import { LoggingService } from '../logging.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  // @Input()
  // account!: { name: string; status: string; };
  // @Input()
  // id!: NumberValueAccessor;

  constructor(private logServ: LoggingService, private AccSer: AccountService){}

  ngOnInit() {
    this.logServ.logStatusChange('INNER COMPONENT');
    this.AccSer.statusUpdate.emit('InnnnActive');
  }

}
