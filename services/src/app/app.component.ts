import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/users.service';
import { AccountService } from './account.service';
import { LoggingService } from './logging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[UserService]
})
export class AppComponent implements OnInit {
  title = 'services';
//  accounts: {name: string, status: string} [] =[];
  constructor(private logServ: LoggingService, private accSer: AccountService){
  //  this.accSer.statusUpdate.subscribe(
  //     // (status: string) => alert('new Status: '+ status)
  //     (status: string) => console.log('subscribe', status)
  //   )
  }

  ngOnInit(){
    // const service = new LoggingService();
    // service.logStatusChange('right');
    // OR
    // this.logServ.logStatusChange('WELOCOME');
    // this.accounts = this.accSer.accounts;
    // this.accSer.addAccount('Testnet', 'Active');
    // this.accSer.addAccount('Mainnet', 'inctive');
    // this.accSer.updateStatus(1, 'Deactivate');
    
  }
}
