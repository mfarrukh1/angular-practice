import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'assignmentPipe',
  pure: false
})
export class AssignmentPipePipe implements PipeTransform {

  transform(value: any): any {
  
      if(value === 'stable'){
        return value.substr(0,4) + ' ..';
      }
   return value;
  }

}
