import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  
  title = 'pipes';
  r: any;

  appStatus = new Promise((resolve, reject)=>{
    setTimeout(()=>{
      resolve('stable');
    },2000);
  });
  

  ngOnInit(){
   this.appStatus.then((res)=>{
    console.log(res);
    this.r = res;
   })
  }
  servers = [
    {
      instanceType: 'medium',
      name: 'production Server',
      status: 'stable',
      started: new Date(15, 6, 2022),
    },
    {
      instanceType: 'large',
      name: 'user database',
      status: 'stable',
      started: new Date(15, 6, 2022),
    },
    {
      instanceType: 'medium',
      name: 'Development Server',
      status: 'offline',
      started: new Date(15, 6, 2022),
    },
    {
      instanceType: 'medium',
      name: 'Testing environment Server',
      status: 'critical',
      started: new Date(15, 6, 2022),
    },
  ];
  filteredStatus = '';
  getStatusClasses(server : {instanceType: string, name: string, status:string, started: Date}){
    return{
      'list-group-item-sucess': server.status === 'stable',
      'list-group-item-warning': server.status === 'offline',
      'list-group-item-danger': server.status === 'critical',

    };
  }

  onAddServer(){

    this.servers.push({
      instanceType: 'small',
      name: 'New Server',
      status: 'stable',
      started: new Date(15, 1, 2017)
    });
  }
}
