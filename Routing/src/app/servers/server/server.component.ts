import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Params, Router } from '@angular/router';
import { ServeersService } from '../serveers.service';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {

  server!: {id:  number, name: string, status: string}
  constructor(private serverService: ServeersService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {

    // we have to add plus sign because our id come in the form of string that's why i added plus sign
    // plus sign convert string into integer before add the plus sign
  //   const id = +this.route.snapshot.params['id'];
  //   this.server = this.serverService.getServer(id);
  //   this.route.params.subscribe((params: Params)=>{
  //     this.server= this.serverService.getServer(+params['id']);
  //     console.log("-----", this.server);
  //   });

  this.route.data.subscribe((data: Data)=>{
    this.server = data['server'];
  });
  }

 

  onEdit(){
    this.router.navigate(['edit'],{relativeTo: this.route, queryParamsHandling:'preserve'});
  }

}
