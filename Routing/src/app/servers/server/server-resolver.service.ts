import { Injectable } from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import { ServeersService } from '../serveers.service';

interface Server{
    id: number;
    name: string;
    status: string;
}
@Injectable()

export class ServerResolver implements Resolve<{id: number, name: string, status: string}> {
    constructor(private serverServices: ServeersService){}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Server> | Promise<Server> | Server {
        return this.serverServices.getServer(+route.params['id']);
    }
}