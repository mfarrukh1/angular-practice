import { TestBed } from '@angular/core/testing';

import { ServeersService } from './serveers.service';

describe('ServeersService', () => {
  let service: ServeersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServeersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
