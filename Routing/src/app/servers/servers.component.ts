import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServeersService } from './serveers.service';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private serverService: ServeersService) { }

  servers: any[]=[]
  ngOnInit(): void {
    this.servers = this.serverService.getServers();
  }
  onReload(){
    this.router.navigate(['/servers'],{relativeTo: this.route});
  }

}
