import { Directive, ElementRef, OnInit, Renderer2, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective implements OnInit{

  @Input() set appUnless(condition: boolean){
    if(!condition){
      this.vcRef.createEmbeddedView(this.templateRef);
    } else {
      this.vcRef.clear();
    }
  }
  constructor(
    private renderer: Renderer2,
    private elRef: ElementRef,
    private templateRef: TemplateRef<any>,
    private vcRef: ViewContainerRef
    ) { }

  ngOnInit() {
    // this.renderer.setStyle(this.elRef.nativeElement,"backgroundColor", "indigo");
    // this.renderer.setStyle(this.elRef.nativeElement,"color", "white",)
  }
}
