import { HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { tap } from "rxjs";

export class LoggingInterceptorService implements HttpInterceptor{
    intercept(req: HttpRequest<any>, next: HttpHandler){
        console.log('Outgoing request');
        console.log('req.url',req.url);
        console.log('req.headers',req.headers);
        return next.handle(req)
        .pipe(
            tap(event => {
                if(event.type === HttpEventType.Response){
                    console.log('Incomingrequest');
                    console.log(event.body);
                }
            })
        );
    }
    
}