import { HttpClient, HttpEventType, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './post.model';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable, Subject, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  error = new Subject<string>();

  constructor(private http: HttpClient) {}
  createAndStorePost(title: string, content: string) {
    const postData: Post = { title: title, content: content };
    this.http
      .post<{ name: string }>(
        'https://ng-http-request-d7d16-default-rtdb.firebaseio.com/posts.json',
        postData,
        {
            observe: 'response'
        }
      )
      .subscribe((resData) => {
        console.log(resData);
      });
  }
  fetchPosts(): Observable<any> {
      let searchParams = new HttpParams();
      searchParams = searchParams.append('print', 'pretty');
      searchParams = searchParams.append('custom', 'key');
    return this.http
      .get<{ [key: string]: Post }>(
        'https://ng-http-request-d7d16-default-rtdb.firebaseio.com/posts.json',
        {
          headers: new HttpHeaders({ 'Custom-Header': 'hy john doe' }),
        //   params: new HttpParams().set('print', 'pretty')
        // Or
          params: searchParams,
        }
      )
      .pipe(
        map((responseData) => {
          const postArray: Post[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              postArray.push({ ...responseData[key], id: key });
            }
          }
          return postArray;
        }),
        catchError((errorRes: any) => {
          return throwError(() => errorRes);
        })
      );
  }

  deletePosts() {
    return this.http.delete(
      'https://ng-http-request-d7d16-default-rtdb.firebaseio.com/posts.json',
      {
        //   observe: 'response'
        //   observe: 'body'
          observe: 'events',
          responseType: 'json'
      }).pipe(
          tap(event =>{
              console.log('event occurred', event);
            //   if(event.type === HttpEventType.Sent){
            //     console.log('f');
            //   }
              if(event.type === HttpEventType.Response){
                console.log(event.body);
              }
          })
      );
  }
}
