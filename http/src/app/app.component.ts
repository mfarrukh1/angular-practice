import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Post } from './post.model';
import { PostsService } from './posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  [x: string]: any;
  loadedPosts: Post[] = [];
  isLoading: boolean = false;
  error = null;

  constructor(private http: HttpClient, private postsService: PostsService) {}
  ngOnInit(): void {
    this.isLoading = true;
    this.postsService.fetchPosts().subscribe({
      next: (posts) => {
        this.isLoading = false;
        this.loadedPosts = posts;
      },
      error: (error) => {
        this.error = error.message;
        console.log('error i am here', error.message);
      },
      complete: () => {
        console.log(' i ran from complete');
      },
    });
  }
  onErr(){
    this.error = null;
  }

  onCreatePost(postData: Post) {
    this.postsService.createAndStorePost(postData.title, postData.content);
  }
  onFetchPosts() {
    this.isLoading = true;

    this.postsService.fetchPosts().subscribe({
      next: (posts) => {
        // success case
        this.isLoading = false;
        this.loadedPosts = posts;
      },
      error: (err) => {
        // failed case
        console.log('err');
      },
      complete: () => {
        console.log(' i ran from complete');
      },
    });
  }
  onClearPosts() {
    this.postsService.deletePosts().subscribe((res) => {
      this.loadedPosts = [];
      console.log('delete response', res);
    });
  }
}
