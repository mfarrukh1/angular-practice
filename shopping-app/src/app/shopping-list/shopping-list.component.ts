import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoggingService } from '../logging.service';
import { Ingredients } from '../shared/ingredients.model';
import { ShoppingListService } from './shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {

  ingredients: Ingredients[] = [];
  private igChangeSub!: Subscription;

  constructor(private shopingService: ShoppingListService, private loggingService: LoggingService) { }
  

  ngOnInit(): void {
    this.ingredients = this.shopingService.getIngredient();
    this.igChangeSub = this.shopingService.ingredientChanged.subscribe((addIng: Ingredients[])=>{
      console.log("chek ingredients", addIng);
      this.ingredients = addIng;
    })
    this.loggingService.printLog('Hello form ShoppingListComponent ngOn/Init');
  }
  ngOnDestroy(): void {
    this.igChangeSub.unsubscribe();
  }
  onEditItem(data: number){
    console.log(data);
    this.shopingService.startedEditing.next(data);
  }

}
