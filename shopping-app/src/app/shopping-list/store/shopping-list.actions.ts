import { Action } from "@ngrx/store";
import { Ingredients } from "src/app/shared/ingredients.model";

export const Add_INGREDIENT= 'ADD_INGREDIENT';

export class AddIngredient implements Action {
    readonly type = Add_INGREDIENT;
    payload!: Ingredients;
    
}
