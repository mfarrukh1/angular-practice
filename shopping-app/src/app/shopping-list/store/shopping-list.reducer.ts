import { Action } from "@ngrx/store";
import { Ingredients } from "../../shared/ingredients.model";
import * as shoppingListAction from "./shopping-list.actions";

const initialState = {
      ingredients : [
        new Ingredients('Apple', 5),
        new Ingredients('Mango', 9),
        new Ingredients('Peach ', 2),
      ]
}

export function shoppingListReducer(state = initialState, action: shoppingListAction.AddIngredient){
    switch (action.type){
        case shoppingListAction.Add_INGREDIENT:
            return {
                ...state,
                ingredients: [...state.ingredients, action.payload]
            };
    }
}