import { Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Ingredients } from 'src/app/shared/ingredients.model';
import { ShoppingListService } from '../shopping-list.service';
import {Form, NgForm} from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  // @ViewChild('nameInput') nameInputRef!: ElementRef;
  // @ViewChild('amountInput') amountInputRef!: ElementRef;

  // @Output() newListEvent = new EventEmitter<any>();
  @ViewChild('f') slForm!: NgForm;

  subscription!: Subscription;
  editMode = false;
  editedItemIndex!: number;
  editedItem!: Ingredients;

  constructor(private shopingService: ShoppingListService) { }

  ngOnInit(): void {
    this.subscription = this.shopingService.startedEditing.subscribe((index: number)=>{
      this.editMode = true;
      this.editedItemIndex = index;
      this.editedItem = this.shopingService.getIngredien(index);
      console.log(this.editedItem);
      this.slForm.setValue({
        name: this.editedItem.name,
        amount: this.editedItem.amount,
      });
      // console.log("fetch the value from form", this.slForm.form.value);
    });
  }
  onAddItem(form : NgForm){
    const value = form.value;
    const newIngredient = new Ingredients(value.name, value.amount);
    if(this.editMode){
      this.shopingService.updateIngredient(this.editedItemIndex, newIngredient);
    }
    else{
      this.shopingService.addIngredient(newIngredient);
    }
    form.reset();
    this.editMode = false;
  }
  onDelete(){
    this.slForm.reset();
    this.editMode =false;
    this.shopingService.deleteIngredient(this.editedItemIndex);
  }
  clearForm(){
    this.slForm.reset();
    this.editMode = false;
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    
  }
}
