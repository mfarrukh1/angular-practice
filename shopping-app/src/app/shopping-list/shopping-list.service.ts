import { Subject } from "rxjs";
import { Ingredients } from "../shared/ingredients.model";

export class ShoppingListService {

    ingredientChanged = new Subject<Ingredients[]>();
    startedEditing = new Subject<number>();

    private ingredients: Ingredients[] =[
        new Ingredients('Apple', 5),
        new Ingredients('Mango', 9),
        new Ingredients('Peach ', 2),
      ]; 
    
    getIngredient(){
        return this.ingredients.slice();
    }
    getIngredien(index: number){
        return  this.ingredients[index];
    }

    addIngredient(ingdt: Ingredients){
       this.ingredients.push(ingdt);
       this.ingredientChanged.next(this.ingredients.slice());
    }
    addIngredients(ingdts: Ingredients[]){
        this.ingredients.push(...ingdts);
        this.ingredientChanged.next(this.ingredients.slice());
     }
    
    updateIngredient(index: number, newIngredient: Ingredients){
        this.ingredients[index] = newIngredient;
        this.ingredientChanged.next(this.ingredients.slice());
    }
    deleteIngredient(index: number){
        this.ingredients.splice(index, 1);
        this.ingredientChanged.next(this.ingredients.slice());
    }
}