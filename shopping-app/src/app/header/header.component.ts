import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { DataStorageService } from '../shared/data-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  title: any = 'Header Component';
  private userSub!: Subscription;
  isAuthenticated = false;

  constructor(
    private dataStore: DataStorageService,
    private authService: AuthService
  ) {}

  ngOnInit(){
    this.userSub = this.authService.user.subscribe(user =>{
        this.isAuthenticated = !!user;
        console.log("!", this.isAuthenticated);
        // console.log("!!", !!user);
        // this.isAuthenticated = !user ? false : true;
    });
    this.userSub = this.authService.user.subscribe();
  }

  onSaveData() {
    this.dataStore.storeRecipe();
  }
  onLogout(){
    this.authService.logout();
  }
  getRecipe() {
    this.dataStore.fetchRecipe().subscribe();
  }
  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

}
