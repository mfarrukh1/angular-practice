import { Ingredients } from "../shared/ingredients.model";

export class Recipe{
    public name: string | any;
    public description: string | any;
    public imagePath: string | any;
    public ingredient!: Ingredients[];

    constructor(name:string, desc: string, imagePath: string, ingredient: Ingredients[]){
        this.name = name;
        this.description = desc;
        this.imagePath = imagePath;
        this.ingredient = ingredient;
    }
}