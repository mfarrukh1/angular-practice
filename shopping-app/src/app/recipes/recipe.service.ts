import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
// import { Subject } from "rxjs";
import { Ingredients } from '../shared/ingredients.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService {
  // recipeSelected = new Subject<Recipe>();
  recipeChanged = new Subject<Recipe[]>();
  recipes: Recipe[] = [];
  // recipes: Recipe[] = [
  //   new Recipe(
  //     'A test Recipe1',
  //     'this is a simply test',
  //     'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ50b2KT_bX6mLVynMrxQc2yZJBRdc8kl0fuw&usqp=CAU',
  //     [new Ingredients('meat', 1), new Ingredients('french fries', 3)]
  //   ),
  //   new Recipe(
  //     'A test Recipe2',
  //     'this is a simply test',
  //     'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ50b2KT_bX6mLVynMrxQc2yZJBRdc8kl0fuw&usqp=CAU',
  //     [new Ingredients('chicken', 4), new Ingredients('burger', 2)]
  //   ),
  //   new Recipe(
  //     'A test Recipe3',
  //     'this is a simply test',
  //     'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ50b2KT_bX6mLVynMrxQc2yZJBRdc8kl0fuw&usqp=CAU',
  //     [new Ingredients('biryani', 1), new Ingredients('tikka', 3)]
  //   ),
  // ];

  constructor(private sl: ShoppingListService) {}

  setRecipes(recipe: Recipe[]){
    this.recipes = recipe;
    this.recipeChanged.next(this.recipes.slice());
  }

  getRecipe() {
    return this.recipes;
  }
  getRecip(index: number) {
    return this.recipes[index];
  }
  addIngredientToShoppingList(ingredient: Ingredients[]) {
    this.sl.addIngredients(ingredient);
  }
  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipeChanged.next(this.recipes.slice());
  }
  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipeChanged.next(this.recipes.slice());
  }
  deleteRecipe(index: number){
    this.recipes.splice(index, 1);
    this.recipeChanged.next(this.recipes.slice());
  }
}
