import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ShoppingListService } from 'src/app/shopping-list/shopping-list.service';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  recipe!: Recipe;
  id!: number;

  constructor(private router: Router, private recipeService: RecipeService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params)=>{
      this.id = +params['id'];
      this.recipe = this.recipeService.getRecip(this.id);
      console.log("--------------------", this.id);
      console.log("--------------------", this.recipe);
    });
    // console.log("--------------------", this.recipe);
  }
  onAddToShoppingList(){
    console.log(this.recipe.ingredient);
    this.recipeService.addIngredientToShoppingList(this.recipe.ingredient);
  }
  onEditRecipe(){
    this.router.navigate(['edit'], {relativeTo: this.route});
    // OR
    // this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});
  }
  onDeleteRecipe(){
    this.router.navigate(['/recipes']);
    this.recipeService.deleteRecipe(this.id);
  }
}
