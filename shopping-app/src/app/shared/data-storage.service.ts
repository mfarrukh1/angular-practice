import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { exhaustMap, map, take, tap } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { Recipe } from '../recipes/recipe.model';
import { RecipeService } from '../recipes/recipe.service';

@Injectable({ providedIn: 'root' })
export class DataStorageService {
  constructor(
    private http: HttpClient,
    private recipeService: RecipeService,
    private authService: AuthService
  ) {}

  storeRecipe() {
    const recipes = this.recipeService.getRecipe();
    this.http
      .put(
        'https://ng-http-request-d7d16-default-rtdb.firebaseio.com/recipe.json',
        recipes
      )
      .subscribe((res) => {
        console.log(res);
      });
  }

  fetchRecipe() {
        return this.http.get<Recipe[]>(
          'https://ng-http-request-d7d16-default-rtdb.firebaseio.com/recipe.json'
        ).pipe(
      map((recipes) => {
        return recipes.map((recipe) => {
          return {
            ...recipe,
            ingredient: recipe.ingredient ? recipe.ingredient : [],
          };
        });
      }),
      tap((recipe) => {
        this.recipeService.setRecipes(recipe);
      })
    );

    // .pipe(
    //   // map((recipes) => {
    //   //   return recipes.map((recipe) => {
    //   //     return {
    //   //       ...recipe,
    //   //       ingredient: recipe.ingredient ? recipe.ingredient : [],
    //   //     };
    //   //   });
    //   // }),
    //   // tap(recipe => {
    //   //     this.recipeService.setRecipes(recipe);
    //   // })
    // )
    //   .subscribe((recipe) => {
    //     this.recipeService.setRecipes(recipe);
    //     console.log('get Recipe', recipe);
    //   });
  }
}
