import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveformComponent } from './reactiveform/reactiveform.component';
import { AssignmentComponent } from './assignment/assignment.component';
import { StripeFormComponent } from './stripe-form/stripe-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ReactiveformComponent,
    AssignmentComponent,
    StripeFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, // this module is use for template driven form
    ReactiveFormsModule, // this module is use for reactive form module
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
