import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidator } from './custom-validator';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.css']
})
export class AssignmentComponent implements OnInit {

  ProjectForm !: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.ProjectForm = new FormGroup({
      'projectName': new FormControl(null, [Validators.required, CustomValidator.invalidProjectName], CustomValidator.asyncInvalidProjectName),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'projectStatus': new FormControl('critical')
    });
  }

  // InvalidProjectName(control: FormControl): {[s: string]: boolean}{
  //   if(control.value === 'Test'){
  //     return {'InvalidProjectName': true};
  //   }
  //   return {'InvalidProjectName': false};
  // }

  onSubmit(){
    console.log(this.ProjectForm.value);
  }

}
