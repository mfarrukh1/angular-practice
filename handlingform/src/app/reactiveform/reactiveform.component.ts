import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reactiveform',
  templateUrl: './reactiveform.component.html',
  styleUrls: ['./reactiveform.component.css']
})
export class ReactiveformComponent implements OnInit {
  
  genders = ['male','female'];
  signupForm!: FormGroup;
  forbiddenUsernames = ['Chris', 'Anna'];
  fa: any;
  constructor() { }

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
        'email': new FormControl(null,[Validators.required, Validators.email], this.forbiddenEmails)
      }),
      'gender': new FormControl('male'),
      'hobbies': new FormArray([]),
    });
    // this.signupForm.valueChanges.subscribe((value)=>{
    //   console.log("value track", value);
    // });
    this.signupForm.statusChanges.subscribe((status)=>{
      console.log("status track", status);
    });
    // update signupForm value
  
    this.signupForm.setValue({
      'userData':{
        'username': 'Max',
        'email': 'max@gmail.com'
      },
      'gender': 'male',
      'hobbies':[]
    });
  
  }

  onSubmit(){
    console.log(this.signupForm.value);
    console.log(this.signupForm);
    this.signupForm.reset();
  }
  onAddHobby(): any{
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(control);
  }
  get hobbiesArrayControl() {
    return (this.signupForm.get('hobbies') as FormArray).controls;
  }

  
  forbiddenNames(control: any): any{
    if(this.forbiddenUsernames.indexOf(control.value) ! == -1){
      return {'nameIsForbidden': true};
    }
    return null;
  }

  forbiddenEmails(control: any): Promise<any> | Observable<any> | any{
    const promise = new Promise<any>((resolve, reject)=>{
      setTimeout(()=>{
        if(control.value === 'test@test.com'){
          resolve({'emailIsForbidden': true});
        } else{
          resolve(null);
        }
      },1500);
    });
    return promise;
  }
  
  

}
