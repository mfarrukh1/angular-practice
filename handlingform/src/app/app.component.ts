import { Component, OnInit, ViewChild } from '@angular/core';
import { Form, NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'handlingform';
  form: Form[] = [];
  defaultQuestion = 'pet';
  answer:any = '';
  genders = ['male','female'];
  submitted : boolean = false;
  user = {
    username: '',
    email: '',
    secretQuestion: '',
    answer: '',
    gender: '',
  }
  @ViewChild('f') signUpForm!: NgForm;

  ngOnInit(): void {
  }



  suggestUserName(){
    const suggestUserName = 'Superuser';
    // this.signUpForm.setValue({
    //   userData:{
    //     username: suggestUserName,
    //     email:''
    //   },
    //   secret:'pet',
    //   questionAnswer:'',
    //   gender:'male',
    // });
    // OR
    this.signUpForm.form.patchValue({
      userData:{
        username: suggestUserName
      }
    });
  }
 
  onSubmit(){
    console.log(this.signUpForm);
    this.submitted = true;
    this.user.username = this.signUpForm.value.userData.username;
    this.user.email = this.signUpForm.value.userData.email;
    this.user.secretQuestion = this.signUpForm.value.secret;
    this.user.answer = this.signUpForm.value.questionAnswer;
    this.user.gender = this.signUpForm.value.gender;
    this.signUpForm.reset();
  }
  
}
