import { FormControl } from "@angular/forms";

export class CardValidator{
    static invalidCardNo(control: any): any{
        const num = control.value;
        
        if(num?.length != 12){
            return { invalidCardNo: true };
        }
        return null;
    }

}